import React from 'react';

function LoginPage() {

  const handleClick = (e) => {
    e.preventDefault();
    const { value } = e.target;

    if (value) {
      console.log(value);
      return;
    }
    console.log(value);

  };

  return (
    <form>
      <input placeholder='Input login' />
      <input placeholder='Input password' />
      <button type='submit' value={1} onClick={handleClick}>Login Ok</button>
      <button type='submit' value={0} onClick={handleClick}>Login False</button>
    </form>
  );
};

export default LoginPage;
