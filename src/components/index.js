import HomePage from './HomePage';
import LoginPage from './LoginPage';
import GuestPage from './GuestPage';
import PrivateRoute from './PrivateRoute';

export {
  PrivateRoute,
  GuestPage,
  HomePage,
  LoginPage,
};
