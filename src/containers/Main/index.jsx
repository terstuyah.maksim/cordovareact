import React from 'react';
import {
  // BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";


import {
  LoginPage,
  GuestPage,
  HomePage,
} from 'components';

function Main() {
  return (
    <>
      <h1>Main Container</h1>
      <Switch>
        <Route path="/homePage">
          <HomePage />
        </Route>
        <Route path="/guestPage">
          <GuestPage />
        </Route>
        <Route path="/loginPage">
          <LoginPage />
        </Route>
      </Switch>
    </>
  );
};

export default Main;
